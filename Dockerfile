FROM openjdk:11-jre-slim-bullseye
COPY target/ex1-1.0.0.jar usr/src/extremo.jar
WORKDIR /usr/src
EXPOSE 9090
ENTRYPOINT [ "java", "-jar", "extremo.jar" ]