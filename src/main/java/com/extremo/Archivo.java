package com.extremo;

import java.io.File;

public class Archivo {
    String ip;
    int puerto;
    String filename;
    File file;

    public Archivo(String ip, int puerto, String fileName) {
        this.filename = fileName;
        this.ip = ip;
        this.puerto = puerto;
    }

    public String getIp() {
        return this.ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPuerto() {
        return this.puerto;
    }

    public void setPuerto(int puerto) {
        this.puerto = puerto;
    }

    public String getFilename() {
        return this.filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public File getFile() {
        return this.file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    @Override
    public String toString() {
        return "{" +
            " ip='" + getIp() + "'" +
            ", port='" + getPuerto() + "'" +
            ", filename='" + getFilename() + "'" +
            "}";
    }

}
