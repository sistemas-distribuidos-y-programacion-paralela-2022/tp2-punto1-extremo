package com.extremo;

import java.net.Socket;
import java.util.Scanner;
import java.net.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Extremo {

    private String address;
    private final Logger log = LoggerFactory.getLogger(ExtremoServerHilo.class);
    String ipServidor, ipMaestro;
    int puertoEsteNodoComoServidor, puertoMaestro;
    String path, Maestro;

    public Extremo() {

        try {

            //Se pide en que ip y puerto escucho a los demas peers y el path de los archivos a compartir
            path = System.getenv("PATH");
            ServerSocket esteNodoServer = new ServerSocket (0);
            puertoEsteNodoComoServidor = esteNodoServer.getLocalPort();

            //Se piden los datos del Maestro
            ipMaestro = System.getenv("MAESTROS_SVC");
            puertoMaestro = Integer.valueOf(System.getenv("PUERTO_SERVIDOR"));
            log.info("Conectando al servidor maestro "+ipMaestro+":"+puertoMaestro);
            log.info("Configuracion del servidor Maestro al que se conecta "+ipMaestro+puertoMaestro);       

            // 1. pongo a escuchar el peer
            // 2. creo un hilo menu cliente
            ExtremoMenu menuCliente = new ExtremoMenu(ipMaestro, puertoMaestro, path, puertoEsteNodoComoServidor);
            // El peer que envio soy yo + dentro de menuCliente debo establecer la conexion con el siguiente peer
            Thread menuThread = new Thread(menuCliente);
            menuThread.start();

            // Se pone a escuchar el peer
            while (true) {
                Socket socketPeer = esteNodoServer.accept();
                ExtremoServerHilo extremoServerHilo = new ExtremoServerHilo(socketPeer, path);
                Thread clientThread = new Thread(extremoServerHilo);
                clientThread.start();
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public static void main(String[] args) {
        try{
            Extremo extremo = new Extremo();
        } catch (Exception e) {
            e.getStackTrace();
        }
    }
}