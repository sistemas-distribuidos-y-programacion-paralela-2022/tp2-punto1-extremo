package com.extremo;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.SimpleTimeZone;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExtremoMenu implements Runnable {

    private final Logger log = LoggerFactory.getLogger(ExtremoServerHilo.class);
    private Gson gson;
    //Socket socket;

    String path;
    String ipMaestro;
    int puertoMaestro, puertoEsteNodoComoServidor;
    Socket socketMaestro;

    public ExtremoMenu(String ip, int puerto, String path, int puertoEsteNodoComoServidor) {
        this.gson = new Gson();
        this.ipMaestro = ip;
        this.puertoMaestro = puerto;
        this.path = path;
        this.puertoEsteNodoComoServidor=puertoEsteNodoComoServidor;
    }

    //TODO metodo para actualizar el peer con con nuevo archivo y se comunica con el server



    @Override
    public void run() {

        boolean flag = true;

        while (flag) {
            System.out.println(
                    "\r\nIngrese la opcion que desee:\r\n1.Listar archivos disponibles\r\n2.Buscar archivos mediante REGEX\r\n3.Descargar archivo (se necesita el nombre clave nombre+ip)\r\n4.Subir archivo\r\n5.Prueba de Carga en un solo servidor (punto 3)\r\n6.Prueba de Carga en el servicio de servidores que balancea (punto 3) \r\n0. Salir");
            Scanner scanner = new Scanner(System.in);

            String value = scanner.nextLine();
            // PEER: 10.10.10.1 - Archivo1 - PORT: 8090
            switch (value) {
                case "0":
                    flag = false;
                break;
                case "1":
                    getListadoCompleto();
                    break;
                case "2":
                    buscarArchivo();
                    break;
                case "3":
                    descargarArchivo();
                    break;
                case "4" :
                    subirArchivo();
                    break;
                case "5" :
                while (true){
                    pruebaDeCarga();
                    log.info("solicitud enviada a un Maestro para que procese. Pulse crl+c para cancelar");
                }
                case "6" :
                while (true){
                    pruebaDeCarga2();
                    log.info("solicitud enviada al servicio del Maestro distribuyendo la carga. Pulse crl+c para cancelar");
                }
                default:
                    break;
            }
        }

    }

    private void pruebaDeCarga2() {
        try {

            Socket socketMaestro = new Socket(ipMaestro, puertoMaestro);
            // Paso 1 - Establecer los canales de comunicación
            PrintWriter canalSalida = new PrintWriter (socketMaestro.getOutputStream(), true);
            // Paso 2 - Enviar petición al servidor 
            // Objects must be stored as JSON (String)
            Message msg = new Message ("pruebaDeCarga2", "" );
            String msgJson = gson.toJson(msg);
            canalSalida.println(msgJson);

            // Paso 5 - Cerrar la conexión
            socketMaestro.close();
				
        } catch (UnknownHostException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
    }

    private void pruebaDeCarga() {
        try {

            Socket socketMaestro = new Socket(ipMaestro, puertoMaestro);
            // Paso 1 - Establecer los canales de comunicación
            PrintWriter canalSalida = new PrintWriter (socketMaestro.getOutputStream(), true);
            // Paso 2 - Enviar petición al servidor 
            // Objects must be stored as JSON (String)
            Message msg = new Message ("pruebaDeCarga1", "" );
            String msgJson = gson.toJson(msg);
            canalSalida.println(msgJson);

            // Paso 5 - Cerrar la conexión
            socketMaestro.close();
				
        } catch (UnknownHostException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }

    }

    private void buscarArchivo() {

        try {
            Scanner scanner = new Scanner(System.in);
            // Paso 1 - Conectarse con el servidor
            // Paso 2 - Establecer los canales de comunicación
            Socket socketMaestro = new Socket(ipMaestro, puertoMaestro);
            BufferedReader canalEntrada = new BufferedReader (new InputStreamReader (socketMaestro.getInputStream()));
            PrintWriter canalSalida = new PrintWriter (socketMaestro.getOutputStream(), true);

            // Paso 3 - Enviar petición al servidor
            // Objects must be stored as JSON (String)
            System.out.println("Ingrese una expresion regular (REGEX) a buscar");
            Message msg = new Message ("getListadoFiltrado", scanner.nextLine());
            String msgJson = gson.toJson(msg);
            canalSalida.println(msgJson);

            // Paso 4 - Recibir la respuesta y descargar el archivo
            Message msgRespuesta = gson.fromJson(canalEntrada.readLine(), Message.class);
            log.info(" MSG RESPUESTA de buscarArchivo: "+msgRespuesta.getBodyJSON());
                //Parseamos el body para conseguir el nombre de archivo
                //Iniciamos la descarga del archivo

            // Paso 5 - Cerrar la conexión
            socketMaestro.close();
        }catch (Exception e){
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }

    private void getListadoCompleto() {

        try {

            Socket socketMaestro = new Socket(ipMaestro, puertoMaestro);
            // Paso 1 - Establecer los canales de comunicación
            BufferedReader canalEntrada = new BufferedReader (new InputStreamReader (socketMaestro.getInputStream()));
            PrintWriter canalSalida = new PrintWriter (socketMaestro.getOutputStream(), true);
            // Paso 2 - Enviar petición al servidor 
            // Objects must be stored as JSON (String)
            Message msg = new Message ("getListadoCompleto", "" );
            String msgJson = gson.toJson(msg);
            canalSalida.println(msgJson);


            // Paso 4 - Recibir la respuesta
            String msgRespuesta = canalEntrada.readLine();
            log.info(" MSG RESPUESTA de getListadoCompleto: "+msgRespuesta);
            // Paso 5 - Cerrar la conexión
            socketMaestro.close();
				
        } catch (UnknownHostException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }

    }
    private void subirArchivo(){
        try {
            Socket socketMaestro = new Socket(ipMaestro, puertoMaestro);
            Scanner scanner = new Scanner(System.in);
            System.out.println("Ingrese Nombre del Archivo a subir");
            String filename = scanner.nextLine();
            String miIP = socketMaestro.getLocalAddress().toString().replace("/", "");//saco la maldita barra
            Archivo archivo = new Archivo(miIP, puertoEsteNodoComoServidor, filename);
            log.info("subiendo "+archivo.getFilename()+"...");

            // Paso 2 - Establecer los canales de comunicación
            BufferedReader canalEntrada = new BufferedReader (new InputStreamReader (socketMaestro.getInputStream()));
            PrintWriter canalSalida = new PrintWriter (socketMaestro.getOutputStream(), true);

            // Paso 3 - Enviar el Peer al servidor con la lista actualizada de archivos.
            Message msg = new Message ("putArchivo", gson.toJson(archivo));
            String msgJson = gson.toJson(msg);
            canalSalida.println(msgJson);
            // Paso 4 - Recibir la respuesta
            String msgRespuestaJson = canalEntrada.readLine();
            Message msgRespuesta = gson.fromJson(msgRespuestaJson,Message.class);
            log.info("MSG RESPUESTA de subirArchivo: "+msgRespuesta.getHeader());

            // Paso 5 - Cerrar la conexión
            socketMaestro.close();

        }catch (Exception e){
            log.error(e.getMessage());
        }

    }

    private void descargarArchivo() {
        byte[] receivedData;
        BufferedInputStream bis;
        BufferedOutputStream bos;
        String file;

        try {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Ingrese Nombre Clave del archivo que desea descargar (nombre:ip)");
            String filename = scanner.nextLine();

            Socket socketMaestro = new Socket(ipMaestro, puertoMaestro);
            // PRIMER PUNTO OBTENGO EL PEER con el puerto y el nombre del archivo
            // Establecer los canales de comunicación
            BufferedReader canalEntrada = new BufferedReader (new InputStreamReader (socketMaestro.getInputStream()));
            PrintWriter canalSalida = new PrintWriter (socketMaestro.getOutputStream(), true);

            // Objects must be stored as JSON (String)
            //String fileName = this.peer.getPath()+"fede.txt";
            Message msg = new Message ("getMetadatosArchivo", filename );
            String msgJson = gson.toJson(msg);
            canalSalida.println(msgJson);

            // - Recibir la respuesta
            String msgRespuestaJson = canalEntrada.readLine();
            log.info(" MSG RESPUESTA de descargarArchivo: "+msgRespuestaJson);
            Message msgRespuesta = gson.fromJson(msgRespuestaJson,Message.class);
            Archivo archivo = gson.fromJson( msgRespuesta.getBodyJSON(), Archivo.class);

            if(socketMaestro.getLocalPort() != archivo.getPuerto()){
                // - Conectarse con el Peer Destino y pedir el recurso
                log.info("conectandose al socket" + archivo.getIp()+archivo.getPuerto());
                Socket socketPeer = new Socket (archivo.getIp(),archivo.getPuerto());
                //server = new Socket (peerDest.getIp(),9092);
                //  - Establecer los canales de comunicación
                canalSalida = new PrintWriter (socketPeer.getOutputStream(), true);

                // Objects must be stored as JSON (String)
                msg = new Message ("getRecurso", gson.toJson(filename) );
                msgJson = gson.toJson(msg);
                canalSalida.println(msgJson);

                //  - Recibir la respuesta, Pongo a escuchar el peer para recibir el archivo
                receivedData = new byte[1024];
                log.info("Se recibio un archivo del extremo.");
                //  Cerrar la conexión

                bis = new BufferedInputStream(socketPeer.getInputStream());
                DataInputStream dis = new DataInputStream(socketPeer.getInputStream());

                // Recibo el nombre del archivo
                file =dis.readUTF();
                file = this.path+ file.substring(file.indexOf('/') + 1, file.length());//TODO no tengo la mas palida idea de que hace aca

                //  Comienzo a escribir el archivo
                int in;
                bos = new BufferedOutputStream(new FileOutputStream(file));
                while ((in = bis.read(receivedData)) != -1) {
                    bos.write(receivedData, 0, in);
                }
                bos.close();
                dis.close();
                socketPeer.close();
                log.info("Archivo recibido y guradado localmente");

                //TODO modificar el metodo para uqe reciba la clave
                descargarArchivo();

                // Cerrar la conexión
                socketMaestro.close();
            }else{
                log.warn("El archivo se encuentra en su Cliente.");
            }


        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }
}
