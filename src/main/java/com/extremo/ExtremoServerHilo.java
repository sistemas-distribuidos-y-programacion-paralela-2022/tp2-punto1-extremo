package com.extremo;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;
import java.io.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//esta clase es el extremo actuando como servidor
public class ExtremoServerHilo implements Runnable {

    private final Logger log = LoggerFactory.getLogger(ExtremoServerHilo.class);

    Socket socketPeer;
    private Gson gson;
    String ip;
    int puerto;
    String path;

    public ExtremoServerHilo(Socket socketPeer, String path) {
        this.socketPeer = socketPeer;
        this.path = path;
        this.gson = new Gson();
    }

    @Override
    public void run() {
        DataOutputStream dataOutputStream;
        try {
            BufferedReader canalEntrada = new BufferedReader(new InputStreamReader(this.socketPeer.getInputStream()));
            dataOutputStream = new DataOutputStream(this.socketPeer.getOutputStream());

            String msgRespuestaJson = canalEntrada.readLine();
            Message msgRespuesta = gson.fromJson(msgRespuestaJson, Message.class);
            log.info("Mensaje recibido: " + msgRespuestaJson);

            if (msgRespuesta.getHeader().startsWith("getRecurso")) {

                String filePath = gson.fromJson(msgRespuesta.getBodyJSON(), String.class);
                // Paso 5 Creo un nuevo socket para enviar el archivo
                int bytes = 0;
                File file = new File(path+filePath);
                log.info("Nuevo archivo creado "+path+filePath);
                FileInputStream fileInputStream = new FileInputStream(file);

                BufferedInputStream bis = new BufferedInputStream(fileInputStream);
                BufferedOutputStream bos = new BufferedOutputStream(this.socketPeer.getOutputStream());

                // enviamos el nombre del archivo
                dataOutputStream.writeUTF(file.getName());

                byte[] byteArray = new byte[8192];
                while ((bytes = bis.read(byteArray)) != -1) {
                    bos.write(byteArray, 0, bytes);
                }

                bis.close();
                bos.close();
                log.info("Archivo enviado.");

            }
            this.socketPeer.close();

        } catch (Exception e) {
            log.error("en extremoHilo"+e.getMessage());
        }
    }

}
