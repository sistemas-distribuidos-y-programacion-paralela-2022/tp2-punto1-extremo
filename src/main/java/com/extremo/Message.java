package com.extremo;

public class Message {
    String header;
    String bodyJSON;
    

    public Message(String header, String bodyJSON) {
        this.header = header;
        this.bodyJSON = bodyJSON;
    }

    public String getHeader() {
        return this.header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getBodyJSON() {
        return this.bodyJSON;
    }

    public void setBodyJSON(String bodyJSON) {
        this.bodyJSON = bodyJSON;
    }
}
